<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/*
 * Helper function to get filemtime of style/script
 * $filename is the path to the file, relative to the root of this theme
 */
function theme_filemtime(string $filename) {
	return filemtime(get_stylesheet_directory() . $filename);
}

// Enqueue styles
function enqueue_styles() {
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );

	$fonts_stylesheet = '/fonts/stylesheet.css';
	wp_enqueue_style('jazy-fonts', get_stylesheet_directory_uri() . $fonts_stylesheet, theme_filemtime($fonts_stylesheet));

	$style = '/jazy-style.css';
  wp_enqueue_style('jazy-style', get_stylesheet_directory_uri() . $style, ['jazy-fonts'], theme_filemtime($style));

	$login = '/jazy-login.css';
	wp_enqueue_style('jazy-login', get_stylesheet_directory_uri() . $login, theme_filemtime($login));
}
add_action('wp_enqueue_scripts', 'enqueue_styles', 1000); // Load last to override

/*
 * Filter function that disables the post navigation buttons
 * at the bottom of posts. This is a filter hook that specifically
 * affects \wp-content\themes\modern\includes\frontend\class-post.php Line 351
 */
function disable_post_bottom_navigation( $args ) {
	return [];
}
add_filter( 'wmhook_modern_post_navigation_post_type', 'disable_post_bottom_navigation' );

/*
 * Hardcoded conditionals for removing the category keyword from the
 * main portfolio and blog pages
 */
function remove_category_heading( $heading ) {
	switch ( $heading ) {
		case 'Category: Portfolio':
			return 'Portfolio';
		case 'Category: Blog':
			return 'Blog';
	}
	return $heading;
}
add_filter( 'get_the_archive_title', 'remove_category_heading' );

// Remove Modern recommended plugins notice
add_filter( 'wmhook_modern_tgmpa_plugins_recommend_plugins', function() { return array(); } );
