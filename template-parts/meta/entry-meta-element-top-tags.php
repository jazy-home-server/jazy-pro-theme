<?php
/**
 * Post meta: Top Tags
 *
 * @copyright  Jazy Llerena
 *
 * Nasty shit done in here. The general aim is to get the last 3 tags of the post and display them normally.
 * Unfortunately 'get_the_tag_list' returns an HTML formatted string, so I have to hack it a bit to get the
 * last 3.
 */





// Requirements check

// Hardcoded %&% as unique separator...
	if ( ! $tags = explode( '%&%', get_the_tag_list( '', '%&%', '', get_the_ID() ) ) ) {
		return;
	}

// Only want the first couple tags
	$tags_count_to_show = 4;
	$top_tags = [];
	for ( $i = 0; $i < count($tags) && $i < $tags_count_to_show; $i++ ) {
		$top_tags[] = $tags[ $i ];
	}
	$top_tags = join(' ', $top_tags);



?>

<div class="entry-meta-element tags-links tags-links__top-meta">
	<span class="entry-meta-description">
		<?php echo esc_html_x( 'Tagged as:', 'Post meta info description: tags list.', 'modern' ); ?>
	</span>
	<?php echo $top_tags; /* WPCS: XSS OK. */ ?>
</div>
